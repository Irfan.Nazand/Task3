﻿using System;
using System.Collections.Generic;

namespace Task3
{
    public class myContacts
    {
        public string firstName { get; set; }
        public string lastName { get; set; }

        public myContacts (string firstname, string lastname)
        {
            firstName = firstname;
            lastName = lastname;
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            // Declare variables
            int restartProgram = 1;
            string restartProg;

            string userInput;

            //Create Dictionary of my contacts
            var myContactsDictionary = new Dictionary<string, myContacts>
            {
                {"Irfan", new myContacts ("Irfan", " Nazand") },
                {"Robert", new myContacts ("Robert", " Lewandowski") },
                {"Manual", new myContacts ("Manual", " Neuer") },
                {"Frank", new myContacts ("Frank", " Ribery") },
                {"Arjen", new myContacts ("Arjen", " Robben") },
            };

            // Output message for user, and wait for input, once received, compare input to key in dictionary and display output
            while (restartProgram == 1)
            {
                Console.Write("Who are you searching for? ");
                userInput = Console.ReadLine();

    
                var contact = myContactsDictionary[userInput];
                Console.WriteLine(contact.firstName + contact.lastName);

                Console.WriteLine("Anyone else? ");
                restartProg = Console.ReadLine();
                int.TryParse(restartProg, out restartProgram);
            };
 
            /**
            Dictionary<string, string> myContacts = new Dictionary<string, string>();

            myContacts.Add("Irfan", "Nazand");
            myContacts.Add("Manual", "Neuer");
            myContacts.Add("Cristiano", "Ronaldo");
            myContacts.Add("Robert", "Lewandowski");
            myContacts.Add("Jerome", "Boateng");
            **/
            //Console.WriteLine();
            // Console.Write("Who are you searching for? ");
           // string test = Console.ReadLine();


                /**
                if (myContacts.ContainsKey(test))
                {
                    Console.WriteLine(myContacts[test]);
                }
                else
                {
                    Console.WriteLine("Does not exist");
                }
                **/

                /**
                foreach (KeyValuePair<string, string> item in myContacts)
                    {
                    if (test == item.Key)
                    {
                        Console.WriteLine("{0} {1}", item.Key.Length, item.Value.Length);
                    } else
                    {
                        Console.WriteLine("Does not exist");
                    }
                       
                    }
                
                //Console.WriteLine(myContacts.GetValueOrDefault(test));
                **/
            }
    }
}
